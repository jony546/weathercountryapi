var express = require('express');
const axios = require('axios');
const redis = require('redis');
var router = express.Router();
const api_key = "96e4052cc7ad9c7ea4cf059089664547";
const google_api_key = "AIzaSyDT6Zth9D-qru8Ju1StBRmASe2-uvVO4Hc";
const client = redis.createClient();

// Print redis errors to the console
client.on('error', (err) => {
  console.log("Error " + err);
});

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET Weather from coordinates */
router.use('/getWeather', function (req, res, next) {
  var lat = req.query.lat;
  var lng = req.query.lng;
  var city = req.query.city;
  const searchUrl = `https://api.darksky.net/forecast/${api_key}/${lat},${lng}`;
  return client.get(`clima:${city}`, (err, result) => {
    if (result) {
      const resultJSON = JSON.parse(result);
      return res.status(200).json(resultJSON);
    } else {
      return axios.get(searchUrl)
        .then(response => {
          const responseJSON = response.data;
          client.setex(`clima:${city}`, 3600, JSON.stringify(responseJSON));
          return res.status(200).json(responseJSON);
        })
        .catch(err => {
          return res.json(err);
        });
    }
  });
});

/* GET Cities from coordinates */
router.use('/getCities', function (req, res, next) {
  var lat = req.query.lat;
  var lng = req.query.lng;
  const searchUrl = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&sensor=false&key=${google_api_key}`;
  return axios.get(searchUrl)
    .then(response => {
      const responseJSON = response.data;
      // Save the Wikipedia API response in Redis store
      client.setex(`clima:${searchUrl}`, 3600, JSON.stringify(responseJSON));
      // Send JSON response to client
      return res.status(200).json(responseJSON);
    })
    .catch(err => {
      return res.json(err);
    });
});


module.exports = router;
